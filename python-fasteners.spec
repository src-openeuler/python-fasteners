%global _empty_manifest_terminate_build 0
Name:		python-fasteners
Version:	0.19
Release:	1
Summary:	A python package that provides useful locks
License:	ASL 2.0
URL:		https://github.com/harlowja/fasteners
Source0:	https://files.pythonhosted.org/packages/5f/d4/e834d929be54bfadb1f3e3b931c38e956aaa3b235a46a3c764c26c774902/fasteners-0.19.tar.gz
BuildArch:	noarch


%description
Cross-platform locks for threads and processes.

%package -n python3-fasteners
Summary:	A python package that provides useful locks
Provides:	python-fasteners = %{version}-%{release}
Obsoletes:      python-fasteners-help <= %{version}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools python3-pip python3-wheel
%description -n python3-fasteners
Cross-platform locks for threads and processes.

%prep
%autosetup -n fasteners-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-fasteners
%{python3_sitelib}/*
%doc CHANGELOG.md
%doc README.md

%changelog
* Tue Dec 05 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.19-1
- Update package to version 0.19

* Thu Apr 27 2023 wangkai <13474090681@163.com> - 0.18-3
- Fix Obsoletes version

* Wed Apr 26 2023 wangkai <13474090681@163.com> - 0.18-2
- Compling package with pyproject
- Obsoletes subpackage python-fasteners-help

* Thu Sep 29 2022 guozhengxin <guozhengxin@kylinos.cn> - 0.18-1
- Upgrade package to version 0.18

* Tue Feb 15 2022 liqiuyu <liqiuyu@kylinos.cn> - 0.14.1-21
- change the BuildRequires:python3-nose to python3-nose2

* Fri Oct 30 2020 jiangxinyu <jiangxinyu@kylinos.cn> - 0.14.1-20
- Init python3-fasteners project
